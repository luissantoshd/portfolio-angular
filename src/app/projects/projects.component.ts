import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'projects',
  template: `
  <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="projects">
    <div class="my-auto px-auto">
      <h2 class="mb-5">{{ title }}</h2>
      <h3 class="mb-0">Aplicações Web</h3>
      <div class="row">

        <div class="col-lg-3" *ngFor="let project of projects | async">
          <div class="card project">
            <div class="card-image">
              <img *ngIf="project.imageUrl" [src]="project.imageUrl ? project.imageUrl : 'assets/img/laptop-stand.png'">
            </div>
            <div class="card-content">
              <p>{{ project.description }}</p>
            </div>
            <div class="card-action">
              <a href="{{ project.link }}" *ngIf="project.link" 
                target="_blank">
                Ver online
                <i class="fa fa-external-link"> </i>
              </a>
            </div>
          </div>
        </div> 

      </div>
    </div>
  </section>
  `,
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  title: string = "Projetos";
  projects: Observable<any>;
  constructor(db: AngularFirestore) {
    this.projects = db.collection('projects').valueChanges();
    this.projects.subscribe((data) => {
      console.log(data)
    })
  }

  ngOnInit() {
  }

}
