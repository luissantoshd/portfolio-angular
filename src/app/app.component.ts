import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [
    '../node_modules/bootstrap/dist/css/bootstrap.css',
    './styles.css',
    './app.component.css'
  ]
})
export class AppComponent implements OnInit {
  _projects: any[];

  constructor(private db: AngularFirestore) { }

  ngOnInit() {
    this.db.collection('projects').valueChanges().subscribe(projects => {
      this._projects = projects;
    })
  }

  scroll(el: HTMLElement) {
    el.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    });
  }
}
